import BN from 'bn.js';
import { web3 } from './web3';

export async function sign(
  from: string,
  to: string,
  gas: BN,
  data: string,
  value: BN,
  key: string,
) {
  let gasPrice = await web3.eth.getGasPrice();
  let chainId = await web3.eth.net.getId();

  const tx = {
    from,
    to,
    gas: gas.toString(),
    gasPrice,
    data,
    value: value.toString(),
    chainId,
  };

  return web3.eth.accounts.signTransaction(tx, `0x${key}`);
}

export async function sendTransaction(signedTransaction: string) {
  return web3.eth.sendSignedTransaction(signedTransaction);
}
