import { web3 } from './web3';
export function validateEthAddress(address: string | null) {
  let notEmpty = !!address;
  return notEmpty && web3.utils.isAddress(address);
}
